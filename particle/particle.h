#pragma once

#include "../main.h"

class Hydrodynamics;

enum PlateletState
{
    isFree       // free particle in blood flow
    , isSeed     // anchored (seed particles at injury site)
    , onBorder   // on thrombus border
    , inThrombus // inside thrombus
    , isDummy    // dummy particle in injury site to simulate high vWF
    //active1, active2, etc.
    , NUM_STATES  // last enum element equals number of elements
};

struct Particle {
    T x, y, z, x_last, y_last, z_last, phi_z, phi_z_last,
      v_x, v_y, v_z, w_z,
      f_x, f_y, f_z, m_z,
      f_x_last, f_y_last;
    T hyd_x, hyd_y;
    unsigned int ident;  // Platelet ID (for vectors of bonds).
    vector<unsigned int> bonds; // IDs of interacting platelets.
    vector<double> bonds_angles;
    float ext_activation;  // Extent of platelet activation.
    int time_activation;  // Time of progress of activation.
    double selfR = particleR; // radius of this specific particle
    PlateletState state;
    bool onceAttached = false;

    // Constants related to particles
    // Morse potential constants
    static double const particleR;
    static double const particleMass;
    static double const k_wall;
    static double const tau_act;
    static const unsigned int redFactorAnchor;
    static const double levelMinActiv;
    static const double delta;

    Particle() {}

    Particle(T x, T y, T z, T x_last, T y_last, T z_last, T phi_z, T phi_z_last,
              unsigned int ident, float ext_activation, int time_activation, PlateletState state=isFree) 
			  : x(x), y(y), z(z), x_last(x_last), y_last(y_last),
               z_last(z_last), phi_z(phi_z), phi_z_last(phi_z_last), ident(ident),
			   ext_activation(ext_activation), time_activation(time_activation), state(state) {
        // Particle is not gong to have more than 15 bonds - I've checked it in old model!
        bonds.reserve(1 << 5);
        bonds_angles.reserve(1 << 5);
    }

    virtual void updateForces(T f_x, T f_y, T f_z, T m_z) = 0;

    virtual void interact_with(Hydrodynamics& flow) = 0;

    virtual void updateDynamics() = 0;

    virtual void updateActivation() = 0;

    T distance(Particle& another){
        return sqrt( (this->x - another.x)*(this->x - another.x)
                   + (this->y - another.y)*(this->y - another.y)
                   + (this->z - another.z)*(this->z - another.z) );
    }

    bool bond_exists(Particle& another){
        return std::find(bonds.begin(), bonds.end(), another.ident) != bonds.end();
    }

    void create_bond(Particle& another);

    double return_angle(Particle& another);

    void kill_bond(Particle& another);

    // The function we use when one platelet leaves zone of calculations
    // to delete all the connections that lead to this platelet
    // function is applied to platelet that is out
    void kill_bond_one_side(Particle& another);

    // Big pseudorandom number. We use it because some of the probabilities
    // can be really small. You need to use random() in LINUX
    //int BigRand();

    ~Particle(){};

    // for storing particle data
    virtual void doPrint(std::ostream& os) const = 0;
    friend std::ostream & operator << (std::ostream& os, const Particle& p) {
        p.doPrint(os);
        return os;
    }
    // for loading particle data
    virtual void loadParticle(std::ifstream&) = 0;
};
