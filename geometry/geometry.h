#pragma once

#include "../particle/particle.h" // not just forward declaration, because at least we need access to particle coordinates

enum GeoType {
    Unknown, // default value which has to be overriden in derived classes
    Thrombosis,
    Hemostasis
};

class Geometry
{
public:
    const GeoType geo_type;
    const size_t n_anchored = 0;

    Geometry() : geo_type(Unknown) {}
    Geometry(GeoType geo_type) : geo_type(geo_type) {}

    std::vector<std::array<double, 2>> border_points;

    /**
    * This method is used to place initially anchored particles for this geometry
    * @param anch_part: vector for anchored particles passed from ParticlesContainer
    * @param next_number: counter of particles
    */
    virtual void placeAnchPart(std::vector<Particle*>& anch_part, unsigned int &next_number) = 0;
    
    /**
    * This method sets coordinates for a newly generated particle, which is passed by reference.
    * It is needed because different geometries may have different inlets for particles.
    */
    virtual std::array<double, 3> getNewParticleCoords() = 0;

    /**
    * This method calculates force between platelet and wall and writes it into platelet
    * @param p: Particle object of type MP, because force is set only for moving particles
    */
    virtual void setWallForce(Particle& p) = 0;
    
    //virtual double distanceToIntersectingBoundary(Particle& p) = 0;

    /**
    * This method is used to determine whether particle is inside or outside computation area
    * For optimization only those edges which do not create force are checked here
    * @param p: Particle object
    */
    virtual bool isOut(Particle& p) = 0;

    /**
    * This function describes updating geometry on one time step
    * @param iter: in case time step value will be needed
    */
    virtual void step(long iter) = 0;

    /**
    * Write geometry description in XML format to use it in ParticleVis
    */
    virtual void writeDescFile() {
		ofstream xml_file("data/desc_PVis.xml", std::ios_base::out);
        xml_file << "<particleset>\n";
		xml_file << "\t<particle count=\"500\">\n"
			<< "\t\t<sphere>\n"
				<< "\t\t\t<radius>" << Particle::particleR << "</radius>\n"
			<< "\t\t</sphere>\n"
		<< "</particle>\n";
        xml_file << "\t<static>\n";
		for(size_t i = 0; i < border_points.size() - 1; ++i) {
            const std::array<double, 2>& op1 = border_points[i];
            const std::array<double, 2>& op2 = border_points[i+1];
            xml_file << "\t\t<line>\n"
                << "\t\t\t<corner1>"
                << "<x>" << op1[0] << "</x>"
                << "<y>" << op1[1] << "</y>"
                << "<z>0</z>" << "</corner1>\n"
                << "\t\t\t<corner2>"
                << "<x>" << op2[0] << "</x>"
                << "<y>" << op2[1] << "</y>"
                << "<z>0</z>" << "</corner2>\n"
            << "\t\t</line>\n";
        }
        xml_file << "\t</static>\n";
        xml_file << "</particleset>\n";
		xml_file.close();
    }
};